# Search Params Expressions

A library for matching and compiling URL search parameter strings
based on a regular pattern.

Essentially, [path-to-regexp](https://www.npmjs.com/package/path-to-regexp) but for query strings.

This library does not care what order the parameters are in.

This library can do the following:

 - Match an exact key-value parameter pair
 - Match a parameter with a specific key and extract its value into a parameter
 - Mark parameters as required or optional
 - Mark parameters as single occurences or arrays
 - Get the remaining parameter pairs that are otherwise unmatched

## Examples


```javascript

const matcher = new SearchParamsExpression('pageSize=:size&pageNumber=:num?');

const values = matcher.match('pageNumber=4&pageSize=20');
// { size: '20', num: '4' }

const values2 = matcher.match('pageSize=100');
// { size: '100' }

const notMatched = matcher.match('colour=yellow');
// null

const str = matcher.compile({ size: '50', page: '3' });
// 'pageSize=50&pageNumber=3

// Full syntax example:
const full = new SearchParamsExpression('action=search&pageSize=:size&pageNumber=:num?&category=:categories+&colour=:colours*&tag=:tags*&:rest...');

full.compile({
	size: 50,
	categories: ['clothing', 'homeware'],
	rest: [['campaign_id', 'productRelaunch']]
});
// action=search&pageSize=50&category=clothing&category=homeware&campaign_id=productRelaunch
```

### Typescript

To get the benefits of typescript you need to add type information when calling constructor, since they're not inferred automatically:

```typescript
const matcher = new SearchParamsExpression<{
	required: 'size',
	optional: 'num',
	some: 'categories'
	many: 'colours'|'tags',
	rest: 'rest'
}>('action=search&pageSize=:size&pageNumber=:num?&category=:categories+&colour=:colours*&tag=:tags*&:rest...');
```


## Syntax

| Name          | Symbol | Variable type       | 
|---------------|--------|---------------------|
| Optional      | ?      | string or undefined |
| At least one  | +      | array               |
| More than one | *      | array or undefined  |
