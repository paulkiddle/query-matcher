import SearchParamsExpression, { Rest } from '../src/main.js';

test('test', () => {
const cm = new SearchParamsExpression<{
	a: string
	b?: string
	c: string[]
	d?: string[]
	e: Rest
}>('many=:d+&some=:c*&optional=:b?&required=:a&fixed=0&:e...')

cm.compile({ a: 'a', c: ['c', 'd'], b: 'b', d: ['d'], e: [['e', 'e']] })

const r = cm.parse('many=v1&some=v2&optional=v3&required=v4&fixed=0&rest=val');

expect(r).toEqual({
	d: ['v1'],
	c: ['v2'],
	b: 'v3',
	a: 'v4',
	e: [['rest', 'val']]
})

expect(cm.count({ a: 'a' })).toEqual(-1);
expect(cm.count({ d: ['d'] })).toEqual(-1);

expect(cm.count({ a: 'a', d: ['c', 'd'] })).toEqual( 3);

expect(cm.count({ a: 'a', c: ['c', 'd'], x: 'x', b: 'b', d: ['d'], e: [['e', 'e']] })).toEqual(6);

expect(() => new SearchParamsExpression('a=:b...')).toThrow()

expect(cm.parse('many=a&required=a')).toBeNull()

expect(cm.parse('required=a&fixed=0')).toBeNull()
expect(cm.parse('many=a&fixed=0')).toBeNull()

expect(() => cm.compile({ a: 'a' } as any)).toThrow();
expect(() => cm.compile({ d: ['d'] } as any)).toThrow();

});
