import CherryPicker from './cherry-picker.js';

export type Rest = [string, string][];

type V = string|string[]|Rest|undefined;
type Options = {
	[s: string]: V
};


const operators = {
	'': {
		optional: false,
		multiple: false
	},
	'?': {
		optional: true,
		multiple: false
	},
	'+': {
		optional: false,
		multiple: true
	},
	'*': {
		optional: true,
		multiple: true
	},
}

export default class SearchParamsExpression<T extends Options> {
	#tokens

	constructor(input: string) {
		this.#tokens = input.split('&').map(
			segment => {
				const rest = segment.match(/^:([a-z0-9%]+)...$/i);
				if(rest) {
					return {
						type: 'rest' as const,
						key: rest[1]
					}
				}

				const [param, spec] = segment.split('=');
				const m = spec.match(/^:([a-z0-9%]+)([?+*]?|\.\.\.)$/i);

				if(!m) {
					return {
						type: 'literal' as const,
						param,
						value: spec
					}
				}

				if(m[2] === '...') {
					throw new Error(`The rest segment must not have a query key; change \`${param}=${spec}\` to just \`${spec}\``)
				}

				
				return {
					type: 'variable' as const,
					key: m[1],
					param,
					...operators[m[2] as keyof typeof operators]
				}
			}
		);

	}

	parse(str: string) {
		const q = new CherryPicker(Array.from(new URLSearchParams(str)));
		const entries = {} as Options;

		for(const token of this.#tokens) {
			if(token.type === 'literal' && !q.pickEntry(token.param, token.value)) {
				return null;
			}

			if(token.type === 'variable') {
				if(token.multiple) {
					const value = q.pickMultiple(token.param);
					if(!token.optional && value.length < 1) {
						return null;
					}
					entries[token.key] = value;
				} else {
					const value = q.pick(token.param);
					if(value == undefined) {
						if(!token.optional) {
							return null;
						}
					} else {
						entries[token.key] = value;
					}
				}
			}

			if(token.type === 'rest') {
				entries[token.key] = Array.from(q.entries)
			}
		}
		return entries as T;
	}

	count(entries: Options) {
		let count = 0;
	
		for(const token of this.#tokens) {
			switch(token.type) {
				case 'literal':
					continue;
				case 'variable':
					if(token.multiple) {
						const v = entries[token.key];
						const values = Array.isArray(v) ? v : [];
						if(values.length > 0) {
							count += values.length;
						} else {
							if(!token.optional) {
								return -1;
							}
						}
					} else {
						if(token.key in entries && token.key != undefined) {
							count ++;
						} else {
							if(!token.optional) {
								return -1;
							}
						}
					}
					break;
				case 'rest':
					const e = entries[token.key];
					const l = Array.isArray(e) ? e.length : 0;
					count += l;
					break;
			}
		}
		return count;
	}

	compile(entries: T) {
		const q = new URLSearchParams();

		for(const token of this.#tokens) {
			switch(token.type) {
				case 'literal':
					q.append(token.param, token.value);
					break;
				case 'variable':
					if(token.multiple) {
						const entriesM = (entries as Record<string, string[]>);
						const values = (token.key in entriesM) ? entriesM[token.key] : [];
						if(values.length > 0) {
							for(const v of values) {
								q.append(token.param, v as string);
							}
						} else {
							if(!token.optional) {
								throw new Error(token.key + ' not provided.')
							}
						}
					} else {
						const entriesS = (entries as Record<string, string>)
						if(token.key in entriesS && token.key != undefined) {
							q.append(token.key, entriesS[token.key]);
						} else {
							if(!token.optional) {
								throw new Error(token.key + ' not provided.')
							}
						}
					}
					break;
				case 'rest':
					const rest = (entries as Record<string, [string, string][]>);
					for(const entry of rest[token.key]) {
						q.append(...entry)
					}
					break;
			}
		}
		return q.toString();
	}
}
