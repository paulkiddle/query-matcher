/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  "transform": {
    "\\.[jt]s?$": [
      "ts-jest",
      {
        "useESM": true
      }
    ]
  },
  "moduleNameMapper": {
    "(.+)\\.js": "$1"
  },
  "extensionsToTreatAsEsm": [
    ".ts"
  ]
};
